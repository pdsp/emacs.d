;;; early-init.el --- Early initialization config
;;;
;;; Commentary:
;;; My early initialization configuration
;;;
;;; Code:

;; Disable package.el in favor of straight.el
;; (setq package-enable-at-startup nil)

(provide 'early-init.el)
;;; early-init.el ends here
